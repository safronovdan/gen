CREATE TABLE `authors` (
    `id`    int(10)     unsigned NOT NULL AUTO_INCREMENT,
    `name`  varchar(70)          NOT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `books` (
    `id`    int(10)  unsigned NOT NULL AUTO_INCREMENT,
    `name`  varchar(70)          NOT NULL,
	`author` int(10) unsigned NOT NULL,
    PRIMARY KEY (`id`),
	FOREIGN KEY (author) REFERENCES authors(id)
);
