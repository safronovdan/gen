<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <?= $this->tag->getTitle() ?>
        <?= $this->tag->stylesheetLink('//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css') ?>
        <meta name="description" content="My books test">
        <meta name="author" content="Dan Safronov">
    </head>
    <body>
		<a href="/index/" class="btn btn-default">home</a>
        <?= $this->getContent() ?>
    </body>
</html>