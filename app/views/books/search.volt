{% for book in page.items %}
    {% if loop.first %}
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
					<th>Author</th>
					<th></th>
					<th></th>
                </tr>
            </thead>
            <tbody>
    {% endif %}

    <tr>
        <td>
            {{ book.id }}
        </td>

        <td>
            {{ book.name }}
        </td>
		
		 <td>
             {{ book.getAuthors().name }}
        </td>

        <td width="7%">
            {{ link_to("books/edit/" ~ book.id, "Edit") }}
        </td>

        <td width="7%">
            {{ link_to("books/delete/" ~ book.id, "Delete") }}
        </td>
    </tr>

    {% if loop.last %}
            </tbody>
            <tbody>
                <tr>
                    <td colspan="7">
                        <div>
                            {{ link_to("books/search", "First" ,"class": "btn btn-default") }}
                            {{ link_to("books/search?page=" ~ page.before, "Previous" ,"class": "btn btn-default") }}
                            {{ link_to("books/search?page=" ~ page.next, "Next" ,"class": "btn btn-default") }}
                            {{ link_to("books/search?page=" ~ page.last, "Last" ,"class": "btn btn-default") }}
                            <span class="help-inline">{{ page.current }} of {{ page.total_pages }}</span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    {% endif %}
{% else %}
    No books are recorded
{% endfor %}