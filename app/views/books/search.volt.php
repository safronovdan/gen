<?php $v39226196671iterated = false; ?><?php $v39226196671iterator = $page->items; $v39226196671incr = 0; $v39226196671loop = new stdClass(); $v39226196671loop->self = &$v39226196671loop; $v39226196671loop->length = count($v39226196671iterator); $v39226196671loop->index = 1; $v39226196671loop->index0 = 1; $v39226196671loop->revindex = $v39226196671loop->length; $v39226196671loop->revindex0 = $v39226196671loop->length - 1; ?><?php foreach ($v39226196671iterator as $book) { ?><?php $v39226196671loop->first = ($v39226196671incr == 0); $v39226196671loop->index = $v39226196671incr + 1; $v39226196671loop->index0 = $v39226196671incr; $v39226196671loop->revindex = $v39226196671loop->length - $v39226196671incr; $v39226196671loop->revindex0 = $v39226196671loop->length - ($v39226196671incr + 1); $v39226196671loop->last = ($v39226196671incr == ($v39226196671loop->length - 1)); ?><?php $v39226196671iterated = true; ?>
    <?php if ($v39226196671loop->first) { ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
					 <th>Author</th>
					 <th></th>
					 <th></th>
                </tr>
            </thead>
            <tbody>
    <?php } ?>

    <tr>
        <td>
            <?= $book->id ?>
        </td>

        <td>
            <?= $book->name ?>
        </td>
		
		 <td>
             <?= $book->getAuthors()->name ?>
        </td>

        <td width="7%">
            <?= $this->tag->linkTo(['books/edit/' . $book->id, 'Edit']) ?>
        </td>

        <td width="7%">
            <?= $this->tag->linkTo(['books/delete/' . $book->id, 'Delete']) ?>
        </td>
    </tr>

    <?php if ($v39226196671loop->last) { ?>
            </tbody>
            <tbody>
                <tr>
                    <td colspan="7">
                        <div>
                            <?= $this->tag->linkTo(['books/search', 'First', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['books/search?page=' . $page->before, 'Previous', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['books/search?page=' . $page->next, 'Next', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['books/search?page=' . $page->last, 'Last', 'class' => 'btn btn-default']) ?>
                            <span class="help-inline"><?= $page->current ?> of <?= $page->total_pages ?></span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    <?php } ?>
<?php $v39226196671incr++; } if (!$v39226196671iterated) { ?>
    No books are recorded
<?php } ?>