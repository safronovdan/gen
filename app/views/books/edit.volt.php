<?= $this->tag->form(['books/save', 'role' => 'form']) ?>

    <ul class="pager">
        <li class="previous pull-left">
            <?= $this->tag->linkTo(['books', '&larr; Go Back']) ?>
        </li>
        <li class="pull-right">
            <?= $this->tag->submitButton(['Save', 'class' => 'btn btn-success']) ?>
        </li>
    </ul>

    <?= $this->getContent() ?>

    <h2>Edit books</h2>

    <fieldset>

        <?php foreach ($form as $element) { ?>

                <div class="form-group">
                    <?= $element->label() ?>
                    <?= $element->render(['class' => 'form-control']) ?>
                </div>
        <?php } ?>

    </fieldset>

</form>