<?= $this->tag->form(['books/search']) ?>

    <h2>
        Search books
    </h2>

    <fieldset>

        <?php foreach ($form as $element) { ?>
            <div class="control-group">
                <?= $element->label(['class' => 'control-label']) ?>

                <div class="controls">
                    <?= $element ?>
                </div>
            </div>
        <?php } ?>

        <div class="control-group">
            <?= $this->tag->submitButton(['Search', 'class' => 'btn btn-primary']) ?>
        </div>

    </fieldset>

<?= $this->tag->endform() ?>