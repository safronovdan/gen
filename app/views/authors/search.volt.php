<?php $v41605200271iterated = false; ?><?php $v41605200271iterator = $page->items; $v41605200271incr = 0; $v41605200271loop = new stdClass(); $v41605200271loop->self = &$v41605200271loop; $v41605200271loop->length = count($v41605200271iterator); $v41605200271loop->index = 1; $v41605200271loop->index0 = 1; $v41605200271loop->revindex = $v41605200271loop->length; $v41605200271loop->revindex0 = $v41605200271loop->length - 1; ?><?php foreach ($v41605200271iterator as $author) { ?><?php $v41605200271loop->first = ($v41605200271incr == 0); $v41605200271loop->index = $v41605200271incr + 1; $v41605200271loop->index0 = $v41605200271incr; $v41605200271loop->revindex = $v41605200271loop->length - $v41605200271incr; $v41605200271loop->revindex0 = $v41605200271loop->length - ($v41605200271incr + 1); $v41605200271loop->last = ($v41605200271incr == ($v41605200271loop->length - 1)); ?><?php $v41605200271iterated = true; ?>
    <?php if ($v41605200271loop->first) { ?>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
					 <th></th>
                    <th></th>
					
                </tr>
            </thead>
            <tbody>
    <?php } ?>

    <tr>
        <td>
            <?= $author->id ?>
        </td>

        <td>
            <?= $author->name ?>
        </td>

        <td width="7%">
            <?= $this->tag->linkTo(['authors/edit/' . $author->id, 'Edit']) ?>
        </td>

        <td width="7%">
            <?= $this->tag->linkTo(['authors/delete/' . $author->id, 'Delete']) ?>
        </td>
    </tr>

    <?php if ($v41605200271loop->last) { ?>
            </tbody>
            <tbody>
                <tr>
                    <td colspan="7">
                        <div>
                            <?= $this->tag->linkTo(['authors/search', 'First', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['authors/search?page=' . $page->before, 'Previous', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['authors/search?page=' . $page->next, 'Next', 'class' => 'btn btn-default']) ?>
                            <?= $this->tag->linkTo(['authors/search?page=' . $page->last, 'Last', 'class' => 'btn btn-default']) ?>
                            <span class="help-inline"><?= $page->current ?> of <?= $page->total_pages ?></span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    <?php } ?>
<?php $v41605200271incr++; } if (!$v41605200271iterated) { ?>
    No authors are recorded
<?php } ?>