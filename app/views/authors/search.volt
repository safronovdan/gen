{% for author in page.items %}
    {% if loop.first %}
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
					 <th></th>
                    <th></th>
					
                </tr>
            </thead>
            <tbody>
    {% endif %}

    <tr>
        <td>
            {{ author.id }}
        </td>

        <td>
            {{ author.name }}
        </td>

        <td width="7%">
            {{ link_to("authors/edit/" ~ author.id, "Edit") }}
        </td>

        <td width="7%">
            {{ link_to("authors/delete/" ~ author.id, "Delete") }}
        </td>
    </tr>

    {% if loop.last %}
            </tbody>
            <tbody>
                <tr>
                    <td colspan="7">
                        <div>
                            {{ link_to("authors/search", "First" ,"class": "btn btn-default") }}
                            {{ link_to("authors/search?page=" ~ page.before, "Previous" ,"class": "btn btn-default") }}
                            {{ link_to("authors/search?page=" ~ page.next, "Next", "class": "btn btn-default") }}
                            {{ link_to("authors/search?page=" ~ page.last, "Last" ,"class": "btn btn-default") }}
                            <span class="help-inline">{{ page.current }} of {{ page.total_pages }}</span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    {% endif %}
{% else %}
    No authors are recorded
{% endfor %}