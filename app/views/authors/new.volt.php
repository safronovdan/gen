<?= $this->getContent() ?>

<?= $this->tag->form(['authors/create']) ?>

    <ul class="pager">
        <li class="previous pull-left">
            <?= $this->tag->linkTo(['authors', '&larr; Go Back']) ?>
        </li>
        <li class="pull-right">
            <?= $this->tag->submitButton(['Save', 'class' => 'btn btn-success']) ?>
        </li>
    </ul>

    <fieldset>

    <?php foreach ($form as $element) { ?>

            <div class="form-group">
                <?= $element->label() ?>
                <?= $element->render(['class' => 'form-control']) ?>
            </div>

    <?php } ?>

    </fieldset>

</form>