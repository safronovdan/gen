<?php
use Phalcon\Mvc\Model;
/**
 * Books
 */
class Books extends Model
{
    /**
     * @var integer
     */
    public $id;
    /**
     * @var string
     */
    public $name;
	
	/**
     * @var integer
    */
    public $author;
	
    /**
     * Books initializer
     */	
    public function initialize()
    {
		$this->belongsTo('author', 'Authors', 'id', array(
			'reusable' => true
		));
    }
}