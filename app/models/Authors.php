<?php
use Phalcon\Mvc\Model;
/**
 * Authors
 */
class Authors extends Model
{
	/**
	 * @var integer
	 */
	public $id;
	/**
	 * @var string
	 */
	public $name;
	/**
	 * Authors initializer
	 */
	public function initialize()
	{	
		
		$this->hasMany('id', 'Books', 'author', array(
        	'foreignKey' => array(
        		'message' => 'Author cannot be deleted because it\'s used in Books'
        	)
        ));
		
	}

}