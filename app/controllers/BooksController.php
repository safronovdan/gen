<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class BooksController extends Controller
{
   protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $params = array_slice($uriParts, 2);
    	return $this->dispatcher->forward(
    		array(
    			'controller' => $uriParts[0],
    			'action' => $uriParts[1],
                'params' => $params
    		)
    	);
    }
     /**
     * The start action, it shows the "search" view
     */
    public function indexAction()
    {
        $this->persistent->searchParams = null;

		$this->view->form = new BooksForm();
    }

    /**
     * Execute the "search" based on the criteria sent from the "index"
     * Returning a paginator for the results
     */
    public function searchAction()
    {
        $numberPage = 1;
		
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Books", $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }
		
        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }
		
        $books = Books::find($parameters);
        if (count($books) == 0) {
            $this->flash->notice("The search did not find any authors");
            //return $this->forward("books/index");
        }
		
        $paginator = new Paginator(array(
            "data"  => $books,
            "limit" => 10,
            "page"  => $numberPage
        ));
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Shows the view to create a "new" book
     */
    public function newAction()
    {
         $this->view->form = new BooksForm(null, array('edit' => true));
    }

    /**
     * Shows the view to "edit" an existing book
     */
    public function editAction($id)
    {
       
        if (!$this->request->isPost()) {
            $book = Books::findFirstById($id);
            if (!$book) {
                $this->flash->error("Book was not found");
				return $this->forward("books/index");
            }
            $this->view->form = new BooksForm($book, array('edit' => true));
        }
    }

    /**
     * Creates a book based on the data entered in the "new" action
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("books/index");
        }
		
        $form = new BooksForm;
        $book = new Books();
        $data = $this->request->getPost();
        if (!$form->isValid($data, $book)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('books/new');
        }
        if ($book->save() == false) {
            foreach ($book->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('books/new');
        }
        $form->clear();
        $this->flash->success("book was created successfully");
        return $this->forward("books/index");
    }

    /**
     * Updates a book based on the data entered in the "edit" action
     */
    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("books/index");
        }
        $id = $this->request->getPost("id", "int");
        $book = Books::findFirstById($id);
        if (!$book) {
            $this->flash->error("book does not exist");
            return $this->forward("books/index");
        }
		
        $form = new BooksForm;
        $this->view->form = $form;
        $data = $this->request->getPost();
        if (!$form->isValid($data, $book)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('books/edit/' . $id);
        }
        if ($book->save() == false) {
            foreach ($author->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('books/edit/' . $id);
        }
        $form->clear();
        $this->flash->success("book was updated successfully");
		
        return $this->forward("books/index");
		
		
    }

    /**
     * Deletes an existing book
     */
    public function deleteAction($id)
    {
        
        $books = Books::findFirstById($id);
        if (!$books) {
            $this->flash->error("book was not found");
            return $this->forward("books/index");
        }
        if (!$books->delete()) {
            foreach ($books->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward("books/search");
        }
        $this->flash->success("book was deleted");
        return $this->forward("books/index");
		
    }
}