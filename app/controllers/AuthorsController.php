<?php

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class AuthorsController extends Controller
{
	
	protected function forward($uri)
    {
        $uriParts = explode('/', $uri);
        $params = array_slice($uriParts, 2);
    	return $this->dispatcher->forward(
    		array(
    			'controller' => $uriParts[0],
    			'action' => $uriParts[1],
                'params' => $params
    		)
    	);
    }
     /**
     * The start action, it shows the "search" view
     */
    public function indexAction()
    {
        $this->persistent->searchParams = null;

		$this->view->form = new AuthorsForm();
    }

    /**
     * Execute the "search" based on the criteria sent from the "index"
     * Returning a paginator for the results
     */
    public function searchAction()
    {
        $numberPage = 1;
		
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, "Authors", $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }
		
        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }
		
        $authors = Authors::find($parameters);
        if (count($authors) == 0) {
            $this->flash->notice("The search did not find any authors");
            //return $this->forward("authors/index");
        }
		
        $paginator = new Paginator(array(
            "data"  => $authors,
            "limit" => 10,
            "page"  => $numberPage
        ));
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Shows the view to create a "new" author
     */
    public function newAction()
    {
         $this->view->form = new AuthorsForm(null, array('edit' => true));
    }

    /**
     * Shows the view to "edit" an existing author
     */
    public function editAction($id)
    {
       
        if (!$this->request->isPost()) {
            $author = Authors::findFirstById($id);
            if (!$author) {
                $this->flash->error("Author was not found");
				return $this->forward("authors/index");
            }
            $this->view->form = new AuthorsForm($author, array('edit' => true));
        }
    }

    /**
     * Creates a author based on the data entered in the "new" action
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("authors/index");
        }
		
        $form = new AuthorsForm;
        $author = new Authors();
        $data = $this->request->getPost();
        if (!$form->isValid($data, $author)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('authors/new');
        }
        if ($author->save() == false) {
            foreach ($author->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('authors/new');
        }
        $form->clear();
        $this->flash->success("author was created successfully");
        return $this->forward("authors/index");
    }

    /**
     * Updates a author based on the data entered in the "edit" action
     */
    public function saveAction()
    {
        if (!$this->request->isPost()) {
            return $this->forward("authors/index");
        }
        $id = $this->request->getPost("id", "int");
        $author = Authors::findFirstById($id);
        if (!$author) {
            $this->flash->error("author does not exist");
            return $this->forward("authors/index");
        }
		
        $form = new AuthorsForm;
        $this->view->form = $form;
        $data = $this->request->getPost();
        if (!$form->isValid($data, $author)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('authors/edit/' . $id);
        }
        if ($author->save() == false) {
            foreach ($author->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('authors/edit/' . $id);
        }
        $form->clear();
        $this->flash->success("Author was updated successfully");
		
        return $this->forward("authors/index");
		
		
    }

    /**
     * Deletes an existing author
     */
    public function deleteAction($id)
    {
        
        $authors = Authors::findFirstById($id);
        if (!$authors) {
            $this->flash->error("author was not found");
            return $this->forward("authors/index");
        }
        if (!$authors->delete()) {
            foreach ($authors->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward("authors/search");
        }
        $this->flash->success("author was deleted");
        return $this->forward("authors/index");
		
    }
}